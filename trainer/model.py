#!/usr/bin/env python
"""This file contains all the model information: the training steps, the batch size and the model iself."""

import tensorflow as tf


def get_training_steps():
    """Returns the number of batches that will be used to train your solution.
    It is recommended to change this value."""
    return 50


def get_batch_size():
    """Returns the batch size that will be used by your solution.
    It is recommended to change this value."""
    return 1


def solution(features, labels, mode):
    """Returns an EstimatorSpec that is constructed using the solution that you have to write below."""
    # Input Layer (a batch of images that have 64x64 pixels and are RGB colored (3)
    input_layer = tf.reshape(features["x"], [-1, 64, 64, 3])
    
    model = tf.keras.models.Sequential([
    # Note the input shape is the desired size of the image 64x64 with 3 bytes color
    # This is the first convolution
    tf.keras.layers.Conv2D(64, (4,4), activation='relu', input_shape=input_layer.shape),
    tf.keras.layers.MaxPooling2D(2, 2),
    # The second convolution
    tf.keras.layers.Conv2D(64, (4,4), activation='relu'),
    tf.keras.layers.MaxPooling2D(2,2),
    # The third convolution
    tf.keras.layers.Conv2D(128, (4,4), activation='relu'),
    tf.keras.layers.MaxPooling2D(2,2),
    # The fourth convolution
    tf.keras.layers.Conv2D(128, (4,4), activation='relu'),
    tf.keras.layers.MaxPooling2D(2,2),
    # Flatten the results to feed into a DNN
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dropout(0.5),
    # 512 neuron hidden layer
    tf.keras.layers.Dense(512, activation='relu'),
    tf.keras.layers.Dense(4, activation='softmax')])
    model.compile(loss = 'categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

    # TODO: Code of your solution

    if mode == tf.estimator.ModeKeys.PREDICT:
        # TODO: return tf.estimator.EstimatorSpec with prediction values of all classes
        predictions=  model.predict(features, batch_size = get_batch_size())
        return predictions

    if mode == tf.estimator.ModeKeys.TRAIN:
        # TODO: Let the model train here
        model.train(features, epoch =get_training_steps(), verbose = 1)
        model.save("rps.h5")
        # TODO: return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)
        return model.EstimatorSpec(mode=mode,loss= 'categorical_crossentropy',train_op= get_training_steps())

    if mode == tf.estimator.ModeKeys.EVAL:
        # The classes variable below exists of an tensor that contains all the predicted classes in a batch
        # TODO: eval_metric_ops = {"accuracy": tf.metrics.accuracy(labels=labels, predictions=classes)}
        eval_metric_ops = model.evaluate (labels=labels, predictions=predictions)
        # TODO: return tf.estimator.EstimatorSpec(mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)
        return model.EstimatorSpec(mode=mode, loss= 'categorical_crossentropy', eval_metric_ops=eval_metric_ops)
